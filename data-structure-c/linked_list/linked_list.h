//
// Created by Paweł Jarosz on 01.06.2024.
//

#ifndef DATA_STRUCTURE_C_LINKED_LIST_HPP
#define DATA_STRUCTURE_C_LINKED_LIST_HPP

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

struct node_t_ {
    uint8_t* buffer;
    struct node* next;
};

typedef struct node_t_ node_t;

typedef struct {
    node_t* guard;
    size_t element_size;
    size_t length;
} linked_list;

bool init_list(linked_list* const list, size_t element_size);
bool push_front(linked_list* const list, uint8_t* buffer);
bool push_back(linked_list* const list, uint8_t* buffer);
bool insert(linked_list* const list, uint8_t* buffer, size_t index);
bool remove_index(linked_list* const list, size_t index);

#endif //DATA_STRUCTURE_C_LINKED_LIST_HPP
