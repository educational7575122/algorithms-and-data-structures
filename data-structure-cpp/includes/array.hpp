//
// Created by Paweł Jarosz on 01.06.2024.
//

#pragma once

#include <initializer_list>
#include <stdexcept>

#include <cstddef>

namespace ds {

    template<typename T, size_t Length>
    class Array {
    public:
        using ArrayType = T;
        using ArrayTypePtr = T*;

        Array() = default;
        explicit Array(const Array<T, Length>& rhs);
        explicit Array(const std::initializer_list<T>& elements);

        const T& front() const noexcept { return m_buffer[0]; }
        T& front() noexcept { return m_buffer[0]; }
        const T& back() const noexcept  { return m_buffer[Length]; }
        T& back() noexcept { return m_buffer[Length]; }

        T& at(size_t index);
        const T& at(size_t index) const;
        T& operator[](size_t index) { return m_buffer[index]; }
        const T& operator[](size_t index) const { return m_buffer[index]; }

        ArrayTypePtr* data() noexcept { return &m_buffer[0]; }
        const ArrayTypePtr* data() const noexcept { return &m_buffer[0]; }

        size_t size() const noexcept { return Length; }

        // old style iterators
        T* begin() { return &front(); }
        const T* begin() const { return &front(); }

        T* end() { return (&back() + 1); }
        const T* end() const { return (&back() + 1); }

    private:
        T m_buffer[Length];
    };


    template<typename T, size_t Length>
    T& Array<T, Length>::at(size_t index) {
        if (index > Length) {
            throw std::out_of_range("Index out of the range");
        }
        return m_buffer[index];
    }

    template<typename T, size_t Length>
    const T& Array<T, Length>::at(size_t index) const {
        if (index > Length) {
            throw std::out_of_range("Index out of the range");
        }
        return m_buffer[index];
    }

    template<typename T, size_t Length>
    Array<T, Length>::Array(const Array<T, Length>& rhs) {
        std::copy(rhs.begin(), rhs.end(), m_buffer.begin());
    }

    template<typename T, size_t Length>
    Array<T, Length>::Array(const std::initializer_list<T>& list) {
        std::copy(list.begin(), list.end(), &m_buffer[0]);
    }
}
